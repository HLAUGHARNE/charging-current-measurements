from os import X_OK
import visa
import time
import csv
import measureTemp

# start of Untitled
rm = visa.ResourceManager()

#   Establish connection to the multimeter - note this will need to be changed if different device is used 
v34465A = rm.open_resource('USB0::0x2A8D::0x0101::MY54503013::0::INSTR')

#   Number of samples 
sampleCount = 1

sampleName = str((input("What is the device number? ")))
versionFileName = sampleName + "currentMeasurement" + ".csv"

#versionFileName = str((input("What is the device number?  "))+ "currentMeasurement" + ".csv")

print(versionFileName)

measuredTemperature = measureTemp.tempValue

#   Create a csv for each sample 
with open(versionFileName, 'w') as f:
    csv_writer = csv.writer(f)
    csv_writer.writerow(["Sample","Time","Temperature","Current"])
    print("csv file created")

    while(sampleCount <= 540):

        #   Measure current 
        currentValue = v34465A.query_ascii_values(':MEASure:CURRent:DC? 10')[0]
        print(currentValue)

        #   Write to csv
        csv_writer = csv.writer(f)
        csv_writer.writerow([versionFileName, measuredTemperature, time.strftime('%Y-%m-%d %H:%M:%S'), currentValue])

        time.sleep(10)
        sampleCount+= 1




v34465A.close()
rm.close()
print("done!")
# end of Untitled
